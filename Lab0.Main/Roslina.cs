﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    class  Roslina
    {
        public int wiek { get; set; }
        public string nazwa { get; set; }
        public int wysokosc { get; set; }
        public int cena { get; set; }

        public Roslina()
        {

        }

        public Roslina(int wiek, string nazwa, int wysokosc, int cena)
        {
            this.cena = cena;
            this.nazwa = nazwa;
            this.wysokosc = wysokosc;
            this.wiek = wiek;
        }
        
        public virtual int ZwrocCene()
        {
            return this.cena + 1;
        }
    }
}
