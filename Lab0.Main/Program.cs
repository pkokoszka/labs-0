﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Program
    {
        static void Main(string[] args)
        {
            Kwiat roza = new Kwiat("czerwony", true, true, 1, "Róża", 10, 100);
            Warzywo cebula = new Warzywo(100, true, 1, "Cebula", 200, 123);

            // dziedziczenie -> oba typy mają dostęp do właściwości klasy bazowej Roślina
            Console.WriteLine(roza.cena);
            Console.WriteLine(cebula.cena);

            // hermetyzacja -> mamy dostęp do prywatnego pola tylko poprzez specjalną właściwość, bez możliwości zmiany.
            Console.WriteLine(roza.czyUlubiony);

            //polimorfizm -> lista typów Roslina może przechowywać typy dziedziczące po Roslina - Kwiat, Warzywo
            List<Roslina> listaRoslin = new List<Roslina>();
            listaRoslin.Add(roza);
            listaRoslin.Add(cebula);

            foreach (var item in listaRoslin)
            {
                Console.WriteLine(item.nazwa);
            }

            Console.ReadKey();
        }
    }
}
