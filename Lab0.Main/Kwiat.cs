﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    class Kwiat : Roslina
    {
        public string kolor { get; set; }
        public bool czyKolce { get; set; }

        private bool _czyUlubiony { get; set; }
        public bool czyUlubiony 
        {
            get 
            {
                return _czyUlubiony;
            }
        }

        public Kwiat()
        {

        }

        public Kwiat(string kolor, bool czyKolce, bool czyUlubiony, int wiek, string nazwa, int wysokosc, int cena):
            base(wiek, nazwa, wysokosc, cena)
        {
            this.kolor = kolor;
            this.czyKolce = czyKolce;
            this._czyUlubiony = czyUlubiony;
        }

        public override int ZwrocCene()
        {
            return this.cena + 5;
        }
    }
}
