﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    class Warzywo : Roslina
    {
        public int waga { get; set; }
        public bool czyJadalne { get; set; }

        public Warzywo()
        {
                
        }
        
        public Warzywo(int waga, bool czyJadalne, int wiek, string nazwa, int wysokosc, int cena):
            base(wiek, nazwa, wysokosc, cena)
        {
            this.waga = waga;
            this.czyJadalne = czyJadalne;
        }

        public override int ZwrocCene()
        {
            return this.cena + 15;
        }
    }
}
