﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class LabDescriptor
    {
        public static Type A = typeof(Roslina);
        public static Type B = typeof(Kwiat);
        public static Type C = typeof(Warzywo);

        public static string commonMethodName = "ZwrocCene";
    }
}
